/**
 * services tabs manipulate
 */
$(`.tab`).click(function () {
	const tabindex = $(`.tabs-caption li`).index(this);
	switchTabs(this, "web-active", "tabs-content", tabindex);
});

function switchTabs(elem, switchClass, contentClass, index) {
	$(elem).addClass(switchClass).siblings().removeClass(switchClass);
	$(`.${contentClass}`).removeClass("content-active").eq(index).addClass("content-active");

}

/**
 * hovering cards
 *
 */
$(function () {
	$('.news-item-content').mouseover(function () {
		$(this).find('.day-square').css('background-color', '#18cfab');
		$(this).find( '.post span').css('color', '#18cfab')
	});
	$('.news-item-content').mouseout(function () {
		$(this).find('.day-square').css('background-color', '');
		$(this).find( '.post span').css('color', '')
	})
});


/**
 *all images
 */

const btnLoadMore = $("#load");
const workImages = $(".cat");
const addHeight = 3;
let counter = 0;
const addGridHeight = 3;
workImages.hide(); // hide category
workImages.slice(0, 12).show(200);

function loadMore() {
	setTimeout(function () {
		const imageType = $(".work-tab.actwork").data("filter");
		$(`.cat${imageType}:hidden`).slice(0, 6).show();
		counter++;

		if (counter === 2) {
			$(btnLoadMore).hide();
			counter = 0;
		}
	}, 2000);
}


btnLoadMore.click(function () {
	loadMore();
});


$('.work-tab').click(function () {
	$(btnLoadMore).hide();
	$(this).addClass('actwork').siblings().removeClass("actwork");
	let imageType = $(this).data("filter");
	console.log(`imageType ${imageType}`);
	$(".cat").hide();
	$(`.cat${imageType}`).slice(0, 8).show();
});